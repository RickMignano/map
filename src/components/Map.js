import React, { useRef } from 'react';
import {VectorMap} from 'react-jvectormap';

const Map = () => {

    let map = useRef('map');
    var markers = [
      {latLng: [53.412910, -8.243890], name: 'Dublin'},
      {latLng: [41.902782, 12.496366], name: 'Rome'},
      {latLng: [45.464664, 9.188540], name: 'Milan'},
      {latLng: [38.116669, 13.36666], name: 'Palermo'}
    ];

    return (
        <>
        <div style={{width: 500, height: 500}}>
            <VectorMap map={'fr_mill'}
                       backgroundColor="#3b96ce"
                       ref={map}
                       containerStyle={{
                           width: '100%',
                           height: '100%'
                       }}
                       containerClassName="map"
                       markers={markers}
                       onRegionClick={(a, b) => {
                           console.log(a, b)
                           console.log(this)
                        }}
              />
          </div>
        </>
    )
}

export default Map;